################################
# Author: Hiroki Okuayama
# Email: hiroki.okuyama at cern.ch
# Date : Feb. 2020
################################

MAKEFLAGS=--warn-undefined-variables
MKDIR := mkdir -p

CXX = g++
ROOTCFLAGS := $(shell root-config --cflags)
ROOTLIBS   := $(shell root-config --libs)
ROOTGLIBS  := $(shell root-config --glibs)

INCLUDE = -I./. $(ROOTCFLAGS) -I./include
LIBS = $(ROOTLIBS)


CFLAGS = -Wall $(INCLUDE) -std=c++11
LDFLAGS = $(LIBS) -Wl,-rpath,$(shell root-config --libdir)

SRCDIR = ./src
OBJDIR = ./build
BINDIR = ./bin
RESDIR = ./result

$(shell $(MKDIR) $(OBJDIR) $(BINDIR) $(RESDIR))

SRC = $(wildcard ${SRCDIR}/*.cxx)
OBJ = $(addprefix $(OBJDIR)/, $(notdir $(SRC:.cxx=.o)))
EXE = $(addprefix $(BINDIR)/, $(notdir $(SRC:.cxx=)))

all: $(EXE) $(OBJ)
$(OBJDIR)/%.o: $(SRCDIR)/%.cxx
	@$(CXX) -c $(CFLAGS) $< -o $@
	@echo "[Compiling] $@"

$(BINDIR)/%: $(OBJDIR)/%.o 
	@$(CXX) $(CFLAGS) $< $(LDFLAGS) -o $@
	@echo "[Linking] $@"

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(EXE)


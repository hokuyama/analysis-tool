#include <fstream>
#include <iostream>
#include <json.hpp>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <sys/stat.h>

#include <ROOT/RDataFrame.hxx>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TCut.h>
#include <TF1.h>

// using json = nlohmann::json;
using json = nlohmann::basic_json<std::map, std::vector, std::string, bool,
                                  std::int32_t, std::uint32_t, float>;

std::string genCutMin         (const std::string branch, std::vector<double> params);
std::string genCutMax         (const std::string branch, std::vector<double> params);
std::string genCutMinMax      (const std::string branch, std::vector<double> params);
std::string removeTwoValues   (const std::string branch, std::vector<double> params); 
std::string genGausDistMinMax (const std::string branch, std::vector<double> params, TTree* ); 

std::string genCutFunc( const json&, const std::string, const TTree* ); 

void printHelp();


template<typename Func, typename... Args>
std::function<std::string(const std::string, std::vector<double>)>
binder( Func* f, Args... args )
{
    // std::placeholders::_1 --> const std::string
    // std::placeholders::_2 --> std::vector<double>
    return std::bind( f, std::placeholders::_1, std::placeholders::_2, args... );
}


template<typename T>
void filter( T& df, std::deque<std::pair<std::string, std::string>>& cuts, TH1* h, TFile* o_file, const int nrow, const int ncol ){
  if ( cuts.size() ) { 
    auto cut = cuts.front();
    cuts.pop_front();
    std::string criteria = cut.first;
    std::string cut_name = cut.second;

    auto c = df.Filter( cut_name.c_str() );
    h -> Fill( criteria.c_str(), *( c.Count() ) );

    auto h_current = c.Histo2D({("h_" + criteria).c_str(), ("h_" + criteria).c_str(), ncol, 0.5, ncol+0.5, nrow, 0.5, nrow+0.5}, "col", "row");

    h_current -> Write();
    
    filter( c, cuts, h, o_file, nrow, ncol );
  }
  else {
    return;
  }
}


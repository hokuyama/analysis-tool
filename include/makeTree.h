#include <fstream>
#include <iostream>
#include <json.hpp>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <time.h>

#include <TFile.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TTree.h>

// using json = nlohmann::json;
using json = nlohmann::basic_json<std::map, std::vector, std::string, bool,
                                  std::int32_t, std::uint32_t, float>;

enum FileType { kDatInt = 0, kDatFloat = 1, kJsonInt = 2, kJsonFloat = 3, kJsonConfig = 4,};

std::string config_value_name[4] = {"Enable", "Hitbus", "InjEn", "TDAC"};

char int_type_file[] = "EnMask,OccupancyMap,StatusMap,Occupancy,NoiseMask,NoiseOccupancy";

char float_type_file[] = "Chi2Map,NoiseMap,ThresholdMap,MeanTotMap,SigmaTotMap";


// Functions

json datToJson(std::string /*filepath*/);

void DatHistogramer( int, int, std::string, TH2* );

void JsonHistogramer( int, int, std::string, TH2* );

void ConfigHistogramer( int, int, std::string, TH2*, std::string, std::string );

void printHelp();

// Classes

class FilePathReader {
  static const int nstring = 200;
  char stage_name[nstring];
  char scan_type[nstring];
  char file_name[nstring];
  char file_type[nstring];
  char chip_name[nstring];
  char result_name[nstring];
  char file_number[nstring] = "";
  char branch_name[nstring] = "";

public:
  FilePathReader(std::string);
  int GetFileType();
  std::string GetBranchName();
};

class ConfigReader {
  std::string data_dir;
  std::string module_name;
  std::string chip_type;
  int chip_num;
  int nint_type = 0;
  int nfloat_type = 0;
  json chip_info;

public:
  ConfigReader(std::string);
  const std::string GetDataDir();
  const std::string GetModuleName();
  const std::string GetChipType();
  const int GetIntTypeFileNum();
  const int GetFloatTypeFileNum();
  const int GetChipRow();
  const int GetChipCol();
  const int GetChipNum();
  const std::string GetRootFileName();
  const std::string GetTreeName();
  const std::string GenFilePath(std::string);
  const json GetChipInfo();
};

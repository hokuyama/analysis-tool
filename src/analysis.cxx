#include <analysis.h>

std::string genCutMin(const std::string branch, std::vector<double> params) { return branch + ">" + std::to_string( params[0] ); }

std::string genCutMax(const std::string branch, std::vector<double> params) { return branch + "<" + std::to_string( params[0] ); }

std::string genCutMinMax(const std::string branch, std::vector<double> params) { return genCutMin(branch, { params[0] } ) + "&&" + genCutMax(branch, { params[1] } ); }

std::string removeOneValue(const std::string branch, std::vector<double> params) { return branch + "!=" + std::to_string( params[0] ); }

std::string removeTwoValues(const std::string branch, std::vector<double> params) { return branch + "!=" + std::to_string( params[0] ) + "&&" + branch + "!=" + std::to_string( params[1] ); }

std::string genGausDistMinMax( const std::string branch, std::vector<double> params, TTree* t ) {

  Float_t val;
  t -> SetBranchStatus("*",0);
  t -> SetBranchStatus( branch.c_str() );
  t -> SetBranchAddress( branch.c_str(), &val );

  Float_t max = t -> GetMaximum( branch.c_str() );

  std::unique_ptr<TH1F> hist( new TH1F( "", "", max, 1 ,max+1 ) );
  std::unique_ptr<TF1> f( new TF1( "fit_func", "gaus", 1, max+1 ) );
  Int_t nevents = t -> GetEntries();

  for ( Int_t ii = 0; ii < nevents; ii++ ){
    t -> GetEntry(ii);
    hist -> Fill( val );
  }

  hist -> Fit("fit_func","0Q","");
  Float_t mean = f -> GetParameter(1);
  Float_t sigma = f -> GetParameter(2);
  hist -> Fit( "fit_func", "0Q", "", mean - sigma, mean + sigma );
  mean = f -> GetParameter(1);
  sigma = f -> GetParameter(2);

  return branch + ">" + std::to_string(mean - params[0]*sigma) + "&&" + branch + "<" + std::to_string(mean + params[0]*sigma);

};


std::string genCutFunc ( const json& criteria, const std::string branchName, TTree* t ){
  
  using FuncType = std::function<std::string(const std::string, std::vector<double>)>;
  
  static std::map< std::string, FuncType > funcs;
  if( !funcs.size() ) {
      funcs["MinBound"]        = genCutMin;
      funcs["MaxBound"]        = genCutMax;
      funcs["MinMaxBound"]     = genCutMinMax;
      funcs["GausMinMaxBound"] = binder(genGausDistMinMax, t);
      funcs["RemoveOneValue"]  = removeOneValue;
      funcs["RemoveTwoValues"] = removeTwoValues;
  }

  const auto& method = criteria["method"];
  const auto& params = criteria["params"];

  // ensure the called method is present in the function map
  assert( funcs.find(method) != funcs.end() );
  
  return funcs.at(method)( branchName, params );

}

int main(int argc, char* argv[]) {

  std::string ana_config = "ana.json";
  std::string module_name = "";
  std::string chip_name = "";
  std::string stage_name = "";
  std::string i_file = "result/Tree.root";
  std::string o_file = "result";
  
  int opt;
  while ( (opt = getopt( argc, argv, "Hhi:o:c:m:f:s:" )) != -1 ){
    switch (opt){
      case 'H':
        printHelp();
        return 0;
        break;
      case 'h':
        printHelp();
        return 0;
        break;
      case 'i':
        i_file = std::string(optarg);
        break;
      case 'o':
        o_file = std::string(optarg);
        break;
      case 'c':
        ana_config = std::string(optarg);
        break;
      case 'm':
        module_name = std::string(optarg);
        break;
      case 'f':
        chip_name = std::string(optarg);
        break;
      case 's':
        stage_name = std::string(optarg);
        break;
      default:
        std::cerr << "-> Error while parsing command line parameters!" << std::endl;
        return -1;
    }
  }

  std::ifstream ifs(ana_config);
  const json cut_info = json::parse(ifs);

  const std::string treefilename = i_file;
  const std::string treename = "QCscan_" + chip_name;

  auto *treefile = TFile::Open( treefilename.c_str(), "READ" );
  auto *tree = dynamic_cast<TTree*>( treefile->Get( treename.c_str() ) );
  const int nrow = tree -> GetMaximum("row");
  const int ncol = tree -> GetMaximum("col");
  const int npixel = tree -> GetEntries();
  const int ncuts = cut_info["stages"][stage_name].size();

  std::deque< std::pair < std::string, std::string > > cuts;

  for (auto &criteria : cut_info["stages"][stage_name]) {
    const std::string variable = criteria["variable"];
    const std::string branch_name = cut_info["leafname"][variable];
    const std::string newCut = genCutFunc( criteria, stage_name + "_" + branch_name, tree );
    cuts.push_back( std::make_pair( criteria["alias"], newCut ));
  }

  std::deque< std::pair < std::string, std::string > > cuts_copy = cuts;

  ROOT::EnableImplicitMT();
  ROOT::RDataFrame df( treename.c_str(), treefilename.c_str() );
  auto c = df;

  auto *output_file = TFile::Open( ( o_file + "/after_analysis.root" ).c_str(), "RECREATE" );
  auto *h_health = new TH1F( "h_healthy_pixel", "# of Healthy Pixels;;Pixels", ncuts+1, 0, ncuts+1 );
  auto *h_bad = new TH1F( "h_bad_pixel", "# of Bad Pixels;;Pixels", ncuts+1, 0, ncuts+1 );
  h_health -> Fill( "Pre_cut", *( c.Count() ) );
  h_bad -> Fill( "Pre_cut", npixel - *( c.Count() ) );

  filter( c, cuts, h_health, output_file, nrow, ncol );

  //create plots
  std::vector< std::pair<std::string, std::pair<std::string, std::string>> > plotinfo;
  gStyle -> SetOptStat(0);
  gStyle -> SetPalette(1);
  int k = mkdir((o_file + "/plots").c_str(),0777);
  auto *c1 = new TCanvas( "", "" );
  for(auto &cut : cuts_copy){
    std::string criteria = cut.first;
    std::string cut_name = cut.second;
    plotinfo.push_back( std::make_pair( criteria, std::make_pair(o_file + "/plots/after_analysis_" + criteria + ".png", cut_name)) );
    auto *h = new TH2D("h_tmp", (criteria + ";Col;Row").c_str(), ncol, 0.5, ncol+0.5, nrow, 0.5, nrow+0.5);
    auto *h_read = (TH2D*)output_file -> Get(("h_" + criteria).c_str());
    for (int i = 0; i < ncol; i++) {
      for (int j = 0; j < nrow; j++) {
        h -> Fill( i+1, j+1, 1 - (h_read -> GetBinContent( i+1, j+1 )));
      }
    }
    h -> Draw("colz");
    c1->SaveAs( ( o_file + "/plots/after_analysis_" + criteria + ".png" ).c_str() );
  }

  h_health->SetMinimum(0);
  h_health->GetYaxis()->SetTitleOffset(1.5);
  h_health->Draw("hist");
  h_health->Write();
  plotinfo.push_back( std::make_pair( "healthy_pixel", std::make_pair(o_file + "/plots/after_analysis_healthy_pixel.png", "")) );
  c1->SaveAs( ( o_file + "/plots/after_analysis_healthy_pixel.png" ).c_str() );

  for(int i=1; i <= ncuts; i++) h_bad->Fill( (cuts_copy.at(i-1).first).c_str(), npixel - (h_health-> GetBinContent(i+1)) );
  h_bad->SetMinimum(0);
  h_bad->GetYaxis()->SetTitleOffset(1.5);
  h_bad->Draw("hist");
  h_bad->Write();
  plotinfo.push_back( std::make_pair( "bad_pixel", std::make_pair(o_file + "/plots/after_analysis_bad_pixel.png", "")) );
  c1->SaveAs( ( o_file + "/plots/after_analysis_bad_pixel.png" ).c_str() );

  //dump pathes of png files to a json file
  json plotlog;
  plotlog["criterias"] = plotinfo;
  std::ofstream o(o_file + "/plotLog.json");
  o << std::setw(4) << plotlog << std::endl;

  //close file
  output_file->Close();
}

void printHelp() {
  std::cout << "Help:" << std::endl;
  std::cout << " -h/-H: Shows this." << std::endl;
  std::cout << " -i <file> : Input ROOT Tree file name(default:'result/Tree.root')" << std::endl;
  std::cout << " -o <directory> : Input output file's name(default:'result')" << std::endl;
  std::cout << " -c <file> : Input path of analysis info file(default:'ana.json')" << std::endl;
  std::cout << " -m <module name> : Input module's serial number " << std::endl;
  std::cout << " -f <chip name>  : Input FE_chip's serial number " << std::endl;
  std::cout << " -s <stage name> : Input stage's name " << std::endl;
}
